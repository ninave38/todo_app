from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.


TASKS = ['nothing']

def index(request):
    if request.method == "POST":
        TASKS.append(request.POST['task'])
    context = {'task_list': TASKS}
    return render(request, 'todo.html', context)


def contact(request):
    return render(request, 'contact.html')


def about(request):
    return render(request, 'about.html')

