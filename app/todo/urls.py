from django.urls import path

from .views import contact, about, index #, get_data

urlpatterns = [
        path('', index, name='index'),
        path('contact/', contact, name='contact'),
#        path('get_data/<str:value>',get_data, name='get_data'),
        path('about/', about, name='about'),
]
